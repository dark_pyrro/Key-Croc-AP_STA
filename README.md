A bash script file that can be used to set different access points (as client/STA) for the Hak5 Key Croc when in arming mode.

This means that you can store several different preset wpa_supplicant.conf files in /etc and activate them using this script based on what AP/WiFi network you want to use at a specific moment.

It may also be useful when troubleshooting WiFi access from the Key Croc.

For example:

/etc/wpa_supplicant.conf-altHome

/etc/wpa_supplicant.conf-altWork01

/etc/wpa_supplicant.conf-altClientX

/etc/wpa_supplicant.conf-altClientY

/etc/wpa_supplicant.conf-altCoffeeShop01

(etc)
